from django.apps import AppConfig


class NewsConfig(AppConfig):
    name = 'news'
    verbose_name = 'Новости' # Для админки


class CategotyConfig(AppConfig):
    name = 'category'
    verbose_name = 'Категории' # Для админки