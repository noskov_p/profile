from django.contrib import admin
from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.utils.safestring import mark_safe

from .models import *


class PostAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())
    class Meta:
        model = Post
        fields = '__all__'


class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    form = PostAdminForm
    # При создании нового объекта в админке форма не будет очищать заполнененые поля
    # Будет кнопка "Сохранить как новый объект"
    save_as = True
    # Набор кнопок - вверху 
    save_on_top = True
    # Какие поля показывать в админке
    list_display = ('id', 'title', 'slug', 'Category', 'created_at', 'get_photo', 'views')
    # Сделаем ссылками эти поля
    list_display_links = ('id', 'title')
    # По каким полям можно будет искать
    search_fields = ('title',)
    # По каким полям фильтр, чтобы выбирать нужные по категориям например
    list_filter = ('Category', 'tags',)
    readonly_fields = ('views', 'created_at', 'get_photo')
    # Какие поля показывать в админке при редактировании этой модели
    fields = ('title', 'slug', 'Category', 'tags', 'content', 'photo', 'get_photo', 'views', 'created_at')

    # Чтобы возвращать картинку не строкой, представлением - картинкой
    def get_photo(self, obj):
        if obj.photo:
            return mark_safe(f'<img src="{obj.photo.url}" width="50">')
        else:
            return '-'
    # Отображение названия этого поля в админке
    get_photo.short_description = 'Фото' 

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}        

admin.site.register(Category, CategoryAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Post, PostAdmin)
