from django.contrib import admin
from .models import GeneralSettings\
    , Lead, StatusLead, TypeLead, WorkingStatusLead, ClosureStatusLead\
    ,Deal, StatusDeal, TypeDeal, WorkingStatusDeal, ClosureStatusDeal\
    ,Project, StatusProject, TypeProject, WorkingStatusProject, ClosureStatusProject\
    ,Stage\
    , Task, StatusTask, TypeTask, WorkingStatusTask, ClosureStatusTask\
    , Employee, Team, TeamMembers, ClientInd, Position\
    , LegalEntity, ClientLE, TypeLE, Division, BankAccount\
    , IntegerCustomField, DecimalCustomField, BooleanCustomField, CharCustomField, DateCustomField, DateTimeCustomField, TextCustomField, URLCustomField

admin.site.register(GeneralSettings)
admin.site.register(Lead)
admin.site.register(StatusLead)
admin.site.register(TypeLead)
admin.site.register(WorkingStatusLead)
admin.site.register(ClosureStatusLead)
admin.site.register(Deal)
admin.site.register(StatusDeal)
admin.site.register(TypeDeal)
admin.site.register(WorkingStatusDeal)
admin.site.register(ClosureStatusDeal)
admin.site.register(Project)
admin.site.register(StatusProject)
admin.site.register(TypeProject)
admin.site.register(WorkingStatusProject)
admin.site.register(ClosureStatusProject)
admin.site.register(Stage)
admin.site.register(Task)
admin.site.register(StatusTask)
admin.site.register(TypeTask)
admin.site.register(WorkingStatusTask)
admin.site.register(ClosureStatusTask)
admin.site.register(Employee)
admin.site.register(Team)
admin.site.register(TeamMembers)
admin.site.register(ClientInd)
admin.site.register(Position)
admin.site.register(LegalEntity)
admin.site.register(ClientLE)
admin.site.register(TypeLE)
admin.site.register(Division)
admin.site.register(BankAccount)
admin.site.register(IntegerCustomField)
admin.site.register(DecimalCustomField)
admin.site.register(BooleanCustomField)
admin.site.register(CharCustomField)
admin.site.register(DateCustomField)
admin.site.register(DateTimeCustomField)
admin.site.register(TextCustomField)
admin.site.register(URLCustomField)


