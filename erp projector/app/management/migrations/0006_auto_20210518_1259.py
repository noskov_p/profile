# Generated by Django 3.1.7 on 2021-05-18 12:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0005_auto_20210518_1255'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booleancustomfield',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_booleancustomfield_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='charcustomfield',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_charcustomfield_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='clientind',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Url'),
        ),
        migrations.AlterField(
            model_name='clientle',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_clientle_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='clientle',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Url'),
        ),
        migrations.AlterField(
            model_name='closurestatusdeal',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_closurestatusdeal_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='closurestatuslead',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_closurestatuslead_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='closurestatusproject',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_closurestatusproject_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='closurestatustask',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_closurestatustask_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='datecustomfield',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_datecustomfield_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='datetimecustomfield',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_datetimecustomfield_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='deal',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_deal_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='deal',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Url'),
        ),
        migrations.AlterField(
            model_name='decimalcustomfield',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_decimalcustomfield_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='division',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_division_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='division',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Url'),
        ),
        migrations.AlterField(
            model_name='employee',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Url'),
        ),
        migrations.AlterField(
            model_name='integercustomfield',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_integercustomfield_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='lead',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_lead_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='lead',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Url'),
        ),
        migrations.AlterField(
            model_name='legalentity',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_legalentity_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='legalentity',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Url'),
        ),
        migrations.AlterField(
            model_name='position',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_position_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='project',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_project_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='project',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Url'),
        ),
        migrations.AlterField(
            model_name='stage',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_stage_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='stage',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Url'),
        ),
        migrations.AlterField(
            model_name='statusdeal',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_statusdeal_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='statuslead',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_statuslead_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='statusproject',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_statusproject_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='statustask',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_statustask_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='task',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_task_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='task',
            name='slug',
            field=models.SlugField(blank=True, max_length=255, null=True, unique=True, verbose_name='Url'),
        ),
        migrations.AlterField(
            model_name='team',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_team_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='textcustomfield',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_textcustomfield_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='typedeal',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_typedeal_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='typele',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_typele_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='typelead',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_typelead_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='typeproject',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_typeproject_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='typetask',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_typetask_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='urlcustomfield',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_urlcustomfield_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='workingstatusdeal',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_workingstatusdeal_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='workingstatuslead',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_workingstatuslead_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='workingstatusproject',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_workingstatusproject_author_related', to='management.employee', verbose_name='Автор'),
        ),
        migrations.AlterField(
            model_name='workingstatustask',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='management_workingstatustask_author_related', to='management.employee', verbose_name='Автор'),
        ),
    ]
