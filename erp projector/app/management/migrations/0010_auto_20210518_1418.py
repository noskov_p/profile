# Generated by Django 3.1.7 on 2021-05-18 14:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0009_auto_20210518_1407'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='clientind',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='clientle',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='deal',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='division',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='employee',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='lead',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='legalentity',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='project',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='stage',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='task',
            name='slug',
        ),
    ]
