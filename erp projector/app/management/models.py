from colorfield.fields import ColorField
from djmoney.models.fields import MoneyField
from django.db import models
from django import forms


# ColorField
# https://github.com/fabiocaccamo/django-colorfield
# ColorField defaults to HEX format but also support HEXA. To set the format: color = ColorField(format='hexa')

class GeneralSettings(models.Model):
    """ Основные настройки портала """
    name_company = models.CharField(max_length=255, verbose_name='Название компании')

    # https://github.com/fabiocaccamo/django-colorfield
    # ColorField defaults to HEX format but also support HEXA. To set the format: color = ColorField(format='hexa')
    corporate_color = ColorField(default='#7267EF')
    corporate_color_background = ColorField(default='#161C25')

    # fileStorageAddress = можно задать какую то папку на сервере как файловое хранилище
    # https://djangodoc.ru/3.1/howto/custom-file-storage/
    # https://www.machinelearningmastery.ru/implementing-a-data-warehouse-with-django-e4856c92f146/
    # https://django-storages.readthedocs.io/en/latest/
    # https://habr.com/ru/company/bitrix/blog/151865/

    # logAddress = 

    class Meta:
        verbose_name = "Основная настройка"
        verbose_name_plural = "Основные настройки"

# -------------------------------------------------------------- АДМИНИСТРАТИВНЫЕ ОБЪЕКТЫ -------------------------------------------------

# ----------------------------------------------- Базовые Абстрактные

class BaseProperties(models.Model):
    """ Базовые свойства  """

    title = models.CharField(max_length=255, verbose_name='Название')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    closed_at = models.DateTimeField(default=None, verbose_name='Дата закрытия', null=True, blank=True)
    author = models.ForeignKey('Employee', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_author_related', null=True, blank=True, verbose_name='Автор')
    description = models.TextField(verbose_name='Описание', null=True, blank=True)
    comment = models.TextField(verbose_name='Комментарий', null=True, blank=True)
    photo = models.ImageField(upload_to='entity_photos/%Y/%m/%d/', blank=True)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True


class BaseColor(models.Model):
    """ Для сущностей имеющих цветовой идентификатор '"""

    color = ColorField(default='#7267EF', verbose_name='Цвет')

    class Meta:
        abstract = True


# class BaseHavingSlug(models.Model):
#     """ Для моделей имеющих slug """
#
#     slug = models.SlugField(max_length=255, verbose_name='Url', unique=True, blank = True, null=True)
#
#     class Meta:
#         abstract = True


class BaseWorkingStatus(models.Model):
    """ Базовая, рабочие статусы """

    index = models.PositiveSmallIntegerField(verbose_name='Порядковый номер', unique=True)
    is_current = models.BooleanField(default=False, verbose_name='Текущий')

    #   Только один рабочий этап в сущности может быть со статусом "Текущий"
    def save(self, *args, **kwargs):
        if self.is_current == True:
            self.__class__.objects.filter(is_current=True).update(is_current=False)
        super().save(*args, **kwargs)

    class Meta:
        abstract = True


class BaseClosureStatus(models.Model):
    """ Базовая, статусы закрытия """

    is_successful = models.BooleanField(default=False, verbose_name='Успешный')

    class Meta:
        abstract = True


class BaseHavingManager(models.Model):
    """ Для классов имеющих управленцев и их помощников """

    # Менеджер - тот кто ставит задачу и следит за ее выполнением
    manager = models.ForeignKey('Employee', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_manager_related', verbose_name='Руководитель',
                                 null=True, blank=True)
    # Помощник менеджера
    assistants = models.ManyToManyField('Employee', related_name='%(app_label)s_%(class)s_assistant_related',
                                        verbose_name='Помощник руководителя', blank=True)

    class Meta:
        abstract = True


class BaseHavingParent(models.Model):
    """ Для сущностей имеющих привязку к родителю """

    lead_parent = models.ForeignKey('self', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_lead_parent_related', verbose_name='В рамках Лида',
                                    null=True, blank=True)
    project_parent = models.ForeignKey('self', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_project_parent_related', verbose_name='В рамках Проекта',
                                       null=True, blank=True)
    deal_parent = models.ForeignKey('self', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_deal_parent_related', verbose_name='В рамках Сделки', null=True,
                                    blank=True)
    stage_parent = models.ForeignKey('self', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_stage_parent_related', verbose_name='В рамках Этапа',
                                     null=True, blank=True)
    task_parent = models.ForeignKey('self', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_task_parent_related', verbose_name='В рамках Задачи', null=True,
                                    blank=True)

    class Meta:
        abstract = True


# ----------------------------------------------- Сущности - объектная модель системы


class BaseEntity(BaseProperties, BaseHavingManager, BaseHavingParent):
    """ Базовая сущность системы
        BaseProperties: title, created_at, closed_at, author, description, comment, photo, photo
        BaseHavingManager: manager, assistants
        BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
    """

    curator = models.ForeignKey('Employee', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_curator_related', verbose_name='Куратор', null=True,
                                blank=True)

    # Рабочие команды - те кто выполняет работу
    working_teams = models.ManyToManyField('Team', related_name='%(app_label)s_%(class)s_teams_related', verbose_name='Команды',
                                           blank=True)
    # Наблюдатели - те кто видят информацию по задаче
    observers = models.ManyToManyField('Employee', related_name='%(app_label)s_%(class)s_observers_related',
                                       verbose_name='Наблюдатели',
                                       blank=True)

    lead_source = models.ForeignKey('Lead', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_lead_source_related', verbose_name='Лид источник',
                                    null=True, blank=True)

    client_LE = models.ForeignKey('ClientLE', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_related', verbose_name='Клиент', null=True)
    #   Изменить после заполнения можно только через согласование
    expenses_p = MoneyField(max_digits=19, decimal_places=4, default_currency='RUB', verbose_name='Стоимость выполнения (план)', null=True, blank=True)
    expenses_f = MoneyField(max_digits=19, decimal_places=4, default_currency='RUB', verbose_name='Стоимость выполнения (факт)', null=True, blank=True)
    #   Расчетная стоимость (от прикрепленных задач, расходов) или задается фиксированной
    expenses_is_calculated = models.BooleanField(default=True, verbose_name='Затраты от расходов')

    #   Изменить после заполнения можно только через согласование
    date_start_work_p = models.DateTimeField(default=None, verbose_name='Дата начала работ (план)', null=True, blank=True)
    date_start_work_f = models.DateTimeField(default=None, verbose_name='Дата начала работ (факт)', null=True, blank=True)
    #   Изменить после заполнения можно только через согласование
    date_end_work_p = models.DateTimeField(default=None, verbose_name='Дата окончания работ (план)', null=True, blank=True)
    date_end_work_f = models.DateTimeField(default=None, verbose_name='Дата окончания работ (факт)', null=True, blank=True)
    # Стоимость - предполагаемого контракта, договора, сделки, проекта. Сколько должно придти оплаты в компанию
    # https://github.com/django-money/django-money
    contract_price_p = MoneyField(max_digits=19, decimal_places=4, default_currency='RUB', verbose_name='Цена (план)', null=True, blank=True)
    # Стоимость предполагаемого контракта факт
    contract_price_f = MoneyField(max_digits=19, decimal_places=4, default_currency='RUB', verbose_name='Цена (факт)', null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        abstract = True


class Lead(BaseEntity):
    """ Лид - то из чего рождается возможность заработать для компании
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseEntity:
            Временные:                  date_start_work_p, date_start_work_f, date_end_work_p, date_end_work_f
            Сотрудники:                 curator, working_team, observers,
            Связи админ. сущностей:     lead_source
            Коммерческие:               client_LE, expenses_p, expenses_f, expenses_is_calculated, contract_price_p, contract_price_f

            __str__.title
        BaseHavingManager: manager, assistants
        BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
    """

    type_lead = models.ForeignKey('TypeLead', on_delete=models.PROTECT, related_name='type_lead', null=True, verbose_name='Тип Лида')

    tender = models.URLField(max_length=300, verbose_name='Тендер', null=True, blank=True)

    working_status_lead = models.ManyToManyField('WorkingStatusLead', related_name='working_status_lead', verbose_name='Рабочие статусы')
    closure_status_lead = models.ForeignKey('ClosureStatusLead', on_delete=models.PROTECT, related_name='closure_status_lead', verbose_name='Статус закрытия',
                                            null=True, blank=True)
    status_lead = models.ForeignKey('StatusLead', on_delete=models.PROTECT, related_name='status_lead',  null=True, verbose_name='Статус Лида')

    class Meta:
        verbose_name = "Лид"
        verbose_name_plural = "Лиды"
        ordering = ['-created_at']


class StatusLead(BaseProperties, BaseColor):
    """ Статус Лида - Важный, Исходящий срок, Промо и т.д.
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
    """

    class Meta:
        verbose_name = "Статус Лида"
        verbose_name_plural = "Статусы Лидов"


class TypeLead(BaseProperties, BaseColor):
    """ Тип Лида
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
    """

    class Meta:
        verbose_name = "Тип Лида"
        verbose_name_plural = "Типы Лидов"


class WorkingStatusLead(BaseProperties, BaseColor, BaseWorkingStatus):
    """ Статусы лида (например: Новый, В работе, Подсчет и тд)
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
        BaseWorkingStatus: index, is_current
    """

    class Meta:
        verbose_name = "Рабочий статус Лида"
        verbose_name_plural = "Рабочие статусы Лидов"
        ordering = ['index']


class ClosureStatusLead(BaseProperties, BaseColor, BaseClosureStatus):
    """ Статус зарытия лида (Успешно, Не успешно, Проиграли по цене и т.д.
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
        BaseClosureStatus: is_successful
    """

    class Meta:
        verbose_name = "Статус закрытия Лида"
        verbose_name_plural = "Статусы закрытия Лидов"
        ordering = ['title']


class Deal(BaseEntity):
    """ Сделка - заключенный договор
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseEntity:
            Временные:                  date_start_work_p, date_start_work_f, date_end_work_p, date_end_work_f
            Сотрудники:                 curator, working_team, observers,
            Связи админ. сущностей:     lead_source
            Коммерческие:               client_LE, expenses_p, expenses_f, expenses_is_calculated, contract_price_p, contract_price_f

            __str__.title
        BaseHavingManager: manager, assistants
        BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
    """

    type_deal = models.ForeignKey('TypeDeal', on_delete=models.PROTECT, related_name='type_deal', null=True, verbose_name='Тип сделки')
    date_of_contract = models.DateField(default=None, verbose_name='Дата подписания договора', null=True, blank=True)
    status_deal = models.ForeignKey('StatusDeal', on_delete=models.PROTECT, related_name='status_deal', null=True, verbose_name='Статус Сделки')

    class Meta:
        verbose_name = "Сделка"
        verbose_name_plural = "Сделки"
        ordering = ['-created_at']


class StatusDeal(BaseProperties, BaseColor):
    """ Статус Сделки - Важная, Исходящий срок, Промо и т.д.
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
    """

    class Meta:
        verbose_name = "Статус Сделки"
        verbose_name_plural = "Статусы Сделок"


class TypeDeal(BaseProperties, BaseColor):
    """ Тип Сделки
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
    """

    class Meta:
        verbose_name = "Тип сделки"
        verbose_name_plural = "Типы сделок"
        ordering = ['title']


class WorkingStatusDeal(BaseProperties, BaseColor, BaseWorkingStatus):
    """ Рабочие статусы сделок (например: Новый, В работе, Подсчет и тд)
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
        BaseWorkingStatus: index, is_current
     """

    class Meta:
        verbose_name = "Рабочий статус Сделки"
        verbose_name_plural = "Рабочие статусы Сделок"
        ordering = ['index']


class ClosureStatusDeal(BaseProperties, BaseColor, BaseClosureStatus):
    """ Статус зарытия лида (Успешно, Не успешно, Проиграли по цене и т.д.
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
        BaseClosureStatus: is_successful
    """

    class Meta:
        verbose_name = "Статус закрытия Сделки"
        verbose_name_plural = "Статусы закрытия Сделок"
        ordering = ['title']


class Project(BaseEntity):
    """ Проект
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseEntity:
           Временные:                  date_start_work_p, date_start_work_f, date_end_work_p, date_end_work_f
            Сотрудники:                 curator, working_team, observers,
            Связи админ. сущностей:     lead_source
            Коммерческие:               client_LE, expenses_p, expenses_f, expenses_is_calculated, contract_price_p, contract_price_f

            __str__.title
        BaseHavingManager: manager, assistants
        BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
    """

    type_project = models.ForeignKey('TypeProject', on_delete=models.PROTECT, related_name='type_project', null=True, verbose_name='Тип Проекта')
    working_status_project = models.ManyToManyField('WorkingStatusProject', related_name='working_status_project', verbose_name='Рабочие статусы')
    closure_status_project = models.ForeignKey('ClosureStatusProject', on_delete=models.PROTECT, related_name='closure_status_project',
                                               verbose_name='Статус закрытия', null=True, blank=True)
    status_project = models.ForeignKey('StatusProject', on_delete=models.PROTECT, related_name='status_project', null=True, verbose_name='Статус Проекта')

    class Meta:
        verbose_name = "Проект"
        verbose_name_plural = "Проекты"
        ordering = ['-created_at']


class StatusProject(BaseProperties, BaseColor):
    """ Статус Сделки - Важная, Исходящий срок, Промо и т.д.
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
    """

    class Meta:
        verbose_name = "Статус Проекта"
        verbose_name_plural = "Статусы Проектов"


class TypeProject(BaseProperties, BaseColor):
    """ Тип Проекта
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
    """

    class Meta:
        verbose_name = "Тип проекта"
        verbose_name_plural = "Типы проектов"
        ordering = ['title']


class WorkingStatusProject(BaseProperties, BaseColor, BaseWorkingStatus):
    """ Рабочие статусы Проекта (например: Новый, В работе, Подсчет и тд)
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
        BaseWorkingStatus: index, is_current
    """

    class Meta:
        verbose_name = "Рабочий статус Проекта"
        verbose_name_plural = "Рабочие статусы Проектов"
        ordering = ['index']


class ClosureStatusProject(BaseProperties, BaseColor, BaseClosureStatus):
    """ Статус зарытия Проекта (Успешно, Не успешно, и т.д. для анализа
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
        BaseClosureStatus: is_successful
    """

    class Meta:
        verbose_name = "Статус закрытия Проекта"
        verbose_name_plural = "Статусы закрытия Проектов"
        ordering = ['title']


class Stage(BaseEntity, BaseWorkingStatus):
    """ Этап  - нумерованная стадия выполнения каких-либо работ
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseEntity:
            Временные:                  date_start_work_p, date_start_work_f, date_end_work_p, date_end_work_f
            Сотрудники:                 curator, working_team, observers,
            Связи админ. сущностей:     lead_source
            Коммерческие:               client_LE, expenses_p, expenses_f, expenses_is_calculated, contract_price_p, contract_price_f

            __str__.title
        BaseWorkingStatus: index, is_current
        BaseHavingManager: manager, assistants
        BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
    """

    class Meta:
        verbose_name = "Этап"
        verbose_name_plural = "Этапы"
        constraints = [
            models.UniqueConstraint(fields=['index', 'project_parent', 'deal_parent', 'stage_parent', 'task_parent'], name='unique_stage')
        ]
        #ordering = ['project_parent', 'deal_parent', 'stage_parent', 'task_parent', 'index']
        #order_with_respect_to = 'project_parent'


class Task(BaseEntity):
    """ Задача - проведение каких либо работ
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseEntity:
            Временные:                  date_start_work_p, date_start_work_f, date_end_work_p, date_end_work_f
            Сотрудники:                 curator, working_team, observers,
            Связи админ. сущностей:     lead_source
            Коммерческие:               client_LE, expenses_p, expenses_f, expenses_is_calculated, contract_price_p, contract_price_f

            __str__.title
        BaseHavingManager: manager, assistants
        BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
    """

    type_task = models.ForeignKey('TypeTask', on_delete=models.PROTECT, related_name='type_task', null=True, verbose_name='Тип Задачи')
    working_status_task = models.ManyToManyField('WorkingStatusTask', related_name='working_status_task', verbose_name='Рабочие статусы')
    closure_status_task = models.ForeignKey('ClosureStatusTask', on_delete=models.PROTECT, related_name='closure_status_task',
                                            verbose_name='Статус закрытия', null=True, blank=True)
    status_task = models.ForeignKey('StatusTask', on_delete=models.PROTECT, related_name='status_task', null=True, verbose_name='Статус Задачи')

    # если не null – связь по срокам
    previous_task = models.ForeignKey('Task', on_delete=models.PROTECT, related_name='previous_task_related', verbose_name='Предыдущая задача', null=True,
                                      blank=True)
    # Как считать стоимость – в деньгах или в часах
    in_money = models.BooleanField(default=True, verbose_name='В деньгах')
    #   Затраченные рабочие часы
    #   Изменить после заполнения можно только через согласование
    working_hours_p = models.PositiveSmallIntegerField(verbose_name='Рабочие часы (план)', unique=True)
    working_hours_f = models.PositiveSmallIntegerField(verbose_name='Рабочие часы (факт)', unique=True)
    # Как считать ФОТ – от плана или от факта
    salary_is_from_fact = models.BooleanField(default=True, verbose_name='В деньгах')
    salary = MoneyField(max_digits=19, decimal_places=4, default_currency='RUB', verbose_name='ФОТ', null=True, blank=True)

    class Meta:
        verbose_name = "Задача"
        verbose_name_plural = "Задачи"
        ordering = ['-created_at']


class StatusTask(BaseProperties, BaseColor):
    """ Статус Сделки - Важная, Исходящий срок, Промо и т.д.
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
    """

    class Meta:
        verbose_name = "Статус Задачи"
        verbose_name_plural = "Статусы Задач"


class TypeTask(BaseProperties, BaseColor):
    """ Тип Задачи
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
    """

    class Meta:
        verbose_name = "Тип задачи"
        verbose_name_plural = "Типы Задач"
        ordering = ['title']


class WorkingStatusTask(BaseProperties, BaseColor, BaseWorkingStatus):
    """ Рабочие Статусы задачи (например: Новая, В работе, На согласовании и тд)
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
        BaseWorkingStatus: index, is_current
    """

    class Meta:
        verbose_name = "Рабочий статус Задачи"
        verbose_name_plural = "Рабочие статусы Задач"
        ordering = ['index']


class ClosureStatusTask(BaseProperties, BaseColor, BaseClosureStatus):
    """ Статус зарытия задачи (Успешно, Не успешно, Проиграли по цене и т.д.
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseColor: color
        BaseClosureStatus: is_successful
    """

    class Meta:
        verbose_name = "Статус закрытия Задачи"
        verbose_name_plural = "Статусы закрытия Задач"
        ordering = ['title']


# ---------------------------------------------------------- ФИЗ ЛИЦА ---------------------------------------------------------------------


class BasePerson(models.Model):
    """ Базовая абстрактная модель физ. лица

    """

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    author = models.ForeignKey('Employee', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_author_related',blank=True, null=True, verbose_name='Автор')

    surname = models.CharField(max_length=50, verbose_name='Фамилия')
    name = models.CharField(max_length=50, verbose_name='Имя')
    patronymic = models.CharField(max_length=50, verbose_name='Отчество')
    birthday = models.DateField(default=None, verbose_name='День рождения', null=True, blank=True)
    photo = models.ImageField(upload_to='person_photos/%Y/%m/%d/', blank=True)
    characteristics = models.TextField(verbose_name='Характеристика',null=True, blank=True)
    phone = forms.RegexField(regex=r'^\+?1?\d{9,15}$')
    email = models.EmailField(max_length=254, verbose_name='Email', null=True, blank=True)
    vk = models.URLField(max_length=300, verbose_name='VK', null=True, blank=True)
    facebook = models.URLField(max_length=300, verbose_name='FaceBook', null=True, blank=True)

    def __str__(self):
        return f'{self.surname} {self.name} {self.patronymic}'

    class Meta:
        abstract = True


class Employee(BasePerson):
    """ Сотрудник компании
        BasePerson: slug, created_at,
                    author, surname, name, patronymic, birthday, photo, characteristics
                    phone, email, vk, facebook
                    __str__.surname+name+patronymic
    """

    hiring_at = models.DateField(default=None, verbose_name='Дата приема на работу', null=True, blank=True)
    dismissal_at = models.DateField(default=None, verbose_name='Дата увольнения', null=True, blank=True)
    fixed_salary = MoneyField(max_digits=19, decimal_places=4, default_currency='RUB', verbose_name='Оклад', null=True, blank=True)
    cost_per_hour = MoneyField(max_digits=19, decimal_places=4, default_currency='RUB', verbose_name='Стоимость часа', null=True, blank=True)
    division = models.ForeignKey('Division', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_division_related', null=True, verbose_name='Подразделение')
    position = models.ForeignKey('Position', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_position_related', null=True, verbose_name='Должность')

    class Meta:
        verbose_name = "Сотрудник"
        verbose_name_plural = "Сотрудники"
        ordering = ['surname']


class Team(BaseProperties, BaseHavingManager, BaseColor):
    """ Команда - для объединения сотрудников в команды
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
                        __str__.title
        BaseHavingManager: manager, assistants
        BaseColor: color
    """
    # Состав команды , дата вхождения в команду, % от ФОТ

    title = models.CharField(max_length=255, verbose_name='Название', default='default_title')
    members = models.ManyToManyField('Employee', related_name='%(app_label)s_%(class)s_staff_related',
                                     verbose_name='Сотрудники', blank=True, through='TeamMembers')
    fot = MoneyField(max_digits=19, decimal_places=4, default_currency='RUB', verbose_name='ФОТ', null=True, blank=True)

    @staticmethod
    def default_title():
        return 'Команда'

    class Meta:
        verbose_name = "Команда"
        verbose_name_plural = "Команды"
        ordering = ['-created_at']


class TeamMembers(models.Model):
    """ Фиксация истории членства в командах
        https://www.cyberforum.ru/blogs/125422/blog5193.html
    """

    employee = models.ForeignKey('Employee', on_delete=models.PROTECT, null=True)
    team = models.ForeignKey('Team', on_delete=models.PROTECT, null=True)
    date_joined = models.DateField(auto_now_add=True, verbose_name='Дата вступления')
    disposal_at = models.DateTimeField(default=None, verbose_name='Дата выбытия', null=True, blank=True)
    # https://coderoad.ru/35907879/Django-процент-поля-формы
    # https://www.sql.ru/forum/82088/kakoy-tip-dannyh-luchshe-ispolzovat-dlya-hraneniya-procentov
    percentage_of_FOT = models.CharField(max_length=3, verbose_name='Процент от ФОТ')

    class Meta:
        verbose_name = "Член команды"
        verbose_name_plural = "Члены команд"

class ClientInd(BasePerson):
    """ Клиент физическое лицо / Сотрудник клиента юр. лица
        BasePerson: slug, created_at,
                    author, surname, name, patronymic, birthday, photo, characteristics
                    phone, email, vk, facebook
                    __str__.surname+name+patronymic
    """

    class Meta:
        verbose_name = "Контактное лицо"
        verbose_name_plural = "Контактное лицо"
        ordering = ['surname']


class Position(BaseProperties):
    """ Должность
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
                        __str__.title
    """

    salary = MoneyField(max_digits=19, decimal_places=4, default_currency='RUB', verbose_name='Оклад', null=True, blank=True)
    bonus = MoneyField(max_digits=19, decimal_places=4, default_currency='RUB', verbose_name='Премия', null=True, blank=True)

    class Meta:
        verbose_name = "Должность"
        verbose_name_plural = "Должности"


# ---------------------------------------------------------- ЮР ЛИЦА ---------------------------------------------------------------------


class BaseLE(BaseProperties):
    """ Базовая абстрактная модель Юридического лица
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
                        __str__.title
    """
    typeLE = models.ForeignKey('TypeLE', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_type_LE', null=True, verbose_name='Тип юр. лица')

    phone = forms.RegexField(regex=r'^\+?1?\d{9,15}$')
    optional_phone_one = forms.RegexField(regex=r'^\+?1?\d{9,15}$')
    optional_phone_two = forms.RegexField(regex=r'^\+?1?\d{9,15}$')
    email = models.EmailField(max_length=254, verbose_name='Email', null=True, blank=True)
    optional_email_one = models.EmailField(max_length=254, verbose_name='Email', null=True, blank=True)
    optional_email_two = models.EmailField(max_length=254, verbose_name='Email', null=True, blank=True)
    site = models.URLField(max_length=300, verbose_name='Сайт', null=True, blank=True)
    vk = models.URLField(max_length=300, verbose_name='VK', null=True, blank=True)
    facebook = models.URLField(max_length=300, verbose_name='FaceBook', null=True, blank=True)

    bank_account = models.ManyToManyField('BankAccount', verbose_name='Банковский счет')

    class Meta:
        abstract = True


class LegalEntity(BaseLE):
    """ Юридическое лицо компании
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
                        __str__.title
        BaseLE: phone , optional_phone_one, optional_phone_two, email, optional_email_one, optional_email_two, vk, facebook
                bank_account
    """

    class Meta:
        verbose_name = "Юр. лицо"
        verbose_name_plural = "Юр. лица"
        ordering = ['title']


class ClientLE(BaseLE):
    """ Клиент юридическое лицо
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
                        __str__.title
        BaseLE: phone , optional_phone_one, optional_phone_two, email, optional_email_one, optional_email_two, vk, facebook
                bank_account
    """

    client_ind = models.ManyToManyField('ClientInd', related_name='client_ind', verbose_name='Контактные лица')

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиент"
        ordering = ['title']


class TypeLE(BaseProperties):
    """ Тип юрю лица (ООО, ИП и т.д.)
            BaseProperties: title, created_at, closed_at, author, description, comment, photo
                            __str__.title

        """

    class Meta:
        verbose_name = "Тип юр. лица"
        verbose_name_plural = "Тип юр. лица"
        ordering = ['title']


class Division(BaseProperties, BaseHavingManager):
    """ Структурное подразделение юр. лица
        BaseProperties: title, created_at, closed_at, author, description, comment, photo
        BaseHavingManager: manager, assistants
    """

    legal_entity = models.ForeignKey('LegalEntity', on_delete=models.PROTECT, related_name='legal_entity', null=True, blank=True, verbose_name='Юр. лицо')
    staff = models.ManyToManyField('Employee', related_name='%(app_label)s_%(class)s_staff_related',
                                   verbose_name='Сотрудники', blank=True)
    division_parent = models.ForeignKey('Division', on_delete=models.PROTECT, related_name='%(app_label)s_%(class)s_division_parent_related',
                                        verbose_name='Головное подразделение', null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Подразделение"
        verbose_name_plural = "Подразделения"
        ordering = ['title']


class BankAccount(models.Model):
    """ Банковский счет """

    title = models.CharField(max_length=255, verbose_name='Название счета')
    recipient = models.CharField(max_length=255, verbose_name='Наименование клиента')
    # https://askdev.ru/q/kak-sdelat-pole-formy-chislovym-tolko-v-django-485535/ - ограничить ввод только на цифры
    payment_account = models.CharField(max_length=20, verbose_name='Расчетный счет')
    inn = models.CharField(max_length=12, verbose_name='ИНН')
    kpp = models.CharField(max_length=9, verbose_name='КПП')
    name_of_bank = models.CharField(max_length=255, verbose_name='Наименование банка')
    bic = models.CharField(max_length=9, verbose_name='БИК')
    correspondent_account = models.CharField(max_length=20, verbose_name='Корр. счет')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Банковский счет"
        verbose_name_plural = "Банковские счета"
        ordering = ['title']


# Комментарий к сущности сделать как добавляющиеся записи

# -------------------------------------------------------- Кастомные поля ---------------------------------------------------------------------


class IntegerCustomField(BaseProperties, BaseHavingParent):
    """ Кастомное поле типа целое число
            BaseProperties: title, created_at, closed_at, author, description, comment, photo
                    __str__.title
            BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
        """

    value = models.IntegerField(verbose_name='Значение', null=True, blank=True)

    class Meta:
        verbose_name = "Пользовательское целое"
        verbose_name_plural = "Пользовательские целые"
        ordering = ['-created_at']


class DecimalCustomField(BaseProperties, BaseHavingParent):
    """ Кастомное поле типа целое число
            BaseProperties: title, created_at, closed_at, author, description, comment, photo
                    __str__.title
            BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
        """

    value = models.DecimalField(max_digits=19, decimal_places=2, verbose_name='Значение', null=True, blank=True)

    class Meta:
        verbose_name = "Пользовательское десятичное"
        verbose_name_plural = "Пользовательские десятичные"
        ordering = ['-created_at']


class BooleanCustomField(BaseProperties, BaseHavingParent):
    """ Кастомное поле типа целое число
            BaseProperties: title, created_at, closed_at, author, description, comment, photo
                    __str__.title
            BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
        """

    value = models.BooleanField(verbose_name='Значение', default=False, null=True, blank=True)

    class Meta:
        verbose_name = "Пользовательское логическое"
        verbose_name_plural = "Пользовательские логические"
        ordering = ['-created_at']


class CharCustomField(BaseProperties, BaseHavingParent):
    """ Кастомное поле типа целое число
            BaseProperties: title, created_at, closed_at, author, description, comment, photo
                    __str__.title
            BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
        """

    value = models.CharField(max_length=255, verbose_name='Значение', null=True, blank=True)

    class Meta:
        verbose_name = "Пользовательское строка"
        verbose_name_plural = "Пользовательские строки"
        ordering = ['-created_at']


class DateCustomField(BaseProperties, BaseHavingParent):
    """ Кастомное поле типа целое число
            BaseProperties: title, created_at, closed_at, author, description, comment, photo
                    __str__.title
            BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
        """

    value = models.DateField(default=None, verbose_name='Значение', null=True, blank=True)

    class Meta:
        verbose_name = "Пользовательское дата"
        verbose_name_plural = "Пользовательские даты"
        ordering = ['-created_at']


class DateTimeCustomField(BaseProperties, BaseHavingParent):
    """ Кастомное поле типа целое число
            BaseProperties: title, created_at, closed_at, author, description, comment, photo
                    __str__.title
            BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
        """

    value = models.DateTimeField(default=None, verbose_name='Значение', null=True, blank=True)

    class Meta:
        verbose_name = "Пользовательское дата и время"
        verbose_name_plural = "Пользовательские даты и время"
        ordering = ['-created_at']


class TextCustomField(BaseProperties, BaseHavingParent):
    """ Кастомное поле типа целое число
            BaseProperties: title, created_at, closed_at, author, description, comment, photo
                    __str__.title
            BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
        """

    value = models.TextField(verbose_name='Значение', null=True, blank=True)

    class Meta:
        verbose_name = "Пользовательское текстовое"
        verbose_name_plural = "Пользовательские текстовые"
        ordering = ['-created_at']


class URLCustomField(BaseProperties, BaseHavingParent):
    """ Кастомное поле типа целое число
            BaseProperties: title, created_at, closed_at, author, description, comment, photo
                    __str__.title
            BaseHavingParent: project_parent, deal_parent, stage_parent, task_parent
        """

    value = models.URLField(max_length=300, verbose_name='Значение', null=True, blank=True)

    class Meta:
        verbose_name = "Пользовательское ссылка"
        verbose_name_plural = "Пользовательские ссылки"
        ordering = ['-created_at']

# Расчетное поле - задание формул между пользовательскими полями
