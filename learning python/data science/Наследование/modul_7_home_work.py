# 1. Напишите класс Fraction для работы с дробями.
# Пусть дробь в нашем классе предстает в виде числитель/знаменатель. # Дробное число должно создаваться по запросу Fraction(a, b), где a – это числитель, а b – знаменатель дроби.

# 2. Добавьте возможность сложения (сложения через оператор сложения) для дроби.
# Предполагается, что операция сложения может проводиться как только между дробями,
# так и между дробью и целым числом. Результат оперции должен быть представлен
# в виде дроби.

# 3. Добавьте возможность взятия разности (вычитания через оператор вычитания)
# для дробей. Предполагается, что операция вычитания может проводиться как только
# для двух дробей, так и для дроби и целого числа. Результат оперции должен быть
# представлен в виде дроби.

# 4. Добавьте возможность умножения (умножения через оператор умножения) для дробей.
# Предполагается, что операция умножения может проводиться как только для двух дробей,
# так и для дроби и целого числа. Результат оперции должен быть представлен в виде дроби.

# 5. Добавьте возможность приведения дроби к целому числу через стандартную функцию int().

# 6. Добавьте возможность приведения дроби к числу с плавающей точкой через стандартную
# функцию float().

# 7. Создайте дочерний класс OperationsOnFraction и добавьте туда собственные
# методы getint и getfloat, которые будут возвращять целую часть дроби и представление
# дроби в виде числа с плавающей точкой соответственно.

class Fraction:

    def __init__(self, numerator, denominator):
        self.numerator = numerator
        self.denominator = denominator

    def __add__(self, other):
        if isinstance(other, Fraction):
            if self.denominator == other.denominator:
                return Fraction(self.numerator + other.numerator, self.denominator)
            else:
                res_numerator = self.numerator * other.denominator + other.numerator * self.denominator
                return  Fraction(res_numerator, self.denominator * other.denominator)
        elif type(other) is int:
            res_numerator = other * self.denominator + self.numerator
            return Fraction(res_numerator, self.denominator)
        else:
            return 'Не подходящая сущность для сложения!'

    def __sub__(self, other):
        if isinstance(other, Fraction):
            if self.denominator == other.denominator:
                return Fraction(self.numerator - other.numerator, self.denominator)
            else:
                res_numerator = self.numerator * other.denominator - other.numerator * self.denominator
                return  Fraction(res_numerator, self.denominator * other.denominator)
        elif type(other) is int:
            res_numerator = other * self.denominator - self.numerator
            return Fraction(res_numerator, self.denominator)
        else:
            return 'Не подходящая сущность для разности!'

    def __mul__(self, other):
        if isinstance(other, Fraction):
            return Fraction(self.numerator * other.numerator, self.denominator * other.denominator)
        elif type(other) is int:
            res_numerator = other * self.numerator
            return Fraction(res_numerator, self.denominator)
        else:
            return 'Не подходящая сущность для перемножения!'

    def __int__(self):
        return self.numerator // self.denominator

    def __float__(self):
        return self.numerator / self.denominator

    def __str__(self):
        return '{}/{}'.format(self.numerator, self.denominator)


class OperationsOnFraction(Fraction):

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)

    def  getint(self):
        return self.numerator // self.denominator

    def  getfloat(self):
        return self.numerator / self.denominator

fraction_1 = OperationsOnFraction(numerator=8, denominator=3)
fraction_2 = Fraction(numerator=1, denominator=4)
print(fraction_1.getfloat())

