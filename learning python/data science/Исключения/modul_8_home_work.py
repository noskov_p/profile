# Есть файл с протоколом регистраций пользователей на сайте (registrations.txt).
# Каждая строка содержит информацию о имени, электронной почте и возрасте человека.
#
# Надо проверить данные из файла, для каждой строки:
#
# +++++++присутсвуют все три поля
# +++++++поле имени содержит только буквы
# +++++++поле email содержит @ и .
# +++++++поле возраст является числом от 10 до 99
# В результате проверки нужно сформировать два файла
#
# registrations_good.log для правильных данных, записывать строки как есть
# registrations_bad.log для ошибочных, записывать строку и вид ошибки.
# Для валидации строки данных написать метод, который может выкидывать исключения:
#
# НЕ присутсвуют все три поля: ValueError
# поле имени содержит НЕ только буквы: NotNameError (кастомное исключение)
# поле email НЕ содержит @ и .(точку): NotEmailError (кастомное исключение)
# поле возраст НЕ является числом от 10 до 99: ValueError Вызов метода обернуть в try-except.

import re

class NotNameError(Exception):

    def __init__(self, message, input_data=None):
        self.message = message
        self.input_data = input_data

    def __str__(self):
        return self.message

def validate(line):
    result_list = line.split(' ')
    surname, email, age = result_list
    if not surname.isalpha():
        raise NotNameError('Поле имени содержит НЕ только буквы')
    if not re.match('(^|\s)[-a-z0-9_.]+@([-a-z0-9]+\.)+[a-z]{2,6}(\s|$)', email):
        raise NotNameError('Не корректный email адрес')
    if not age.isdigit() or not (10 < int(age) < 99):
         raise ValueError('Не корректный возраст - ' + age)

registrations_good = []
registrations_bad = []

# Чтение из файла
try:
    with open('registrations_.txt', 'r', encoding='utf-8') as ff:
        for line in ff:
            try:
                validate(line[:-1])
                registrations_good.append(line)
            except (ValueError, NotNameError) as exc:
                if 'unpack' in exc.args[0]:
                    registrations_bad.append(line[:-1] + ' - в строке не хватает данных\n')
                else:
                    registrations_bad.append(line[:-1] + ' - ' + exc.args[0] + '\n')

except FileNotFoundError as exc:
    print(f'Файл с регистрационными данными не найден: {exc}')

# Запись битых данных
with open('registrations_bad.log', 'w', encoding='utf-8') as ff:
    ff.writelines(registrations_bad)

# Запись хороших данных
with open('registrations_good.log', 'w', encoding='utf-8') as ff:
    ff.writelines(registrations_good)