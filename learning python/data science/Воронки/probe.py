import csv

with open('click_stream3.csv', mode='r') as csv_file:  # открываем файл
    csv_reader = csv.DictReader(csv_file)  # читаем файл
    funnel = {}

    for row in csv_reader:
        # print(row.items())
        month = list(row.items())[2][1].split('-')[1]
        gender = list(row.items())[4][1]
        device = list(row.items())[3][1]
        page = list(row.items())[1][1]

        if month not in funnel:
            funnel[month] = {}

        if gender not in funnel[month]:
            funnel[month][gender] = {}

        if device not in funnel[month][gender]:
            funnel[month][gender][device] = {}

        if page not in funnel[month][gender][device]:
            funnel[month][gender][device][page] = 1
        else:
            funnel[month][gender][device][page] += 1
for month in funnel:
    print(month)
    for gender in funnel[month]:
        print(' ' + gender)
        for device in funnel[month][gender]:
            print('   ' + device)
            for info in funnel[month][gender][device]:
                print(f'    {info} - {funnel[month][gender][device][info]} - {round(funnel[month][gender][device][info] / funnel[month][gender][device]["1_home_page"] * 100, 2 )}%')

 #   print(funnel[month])
