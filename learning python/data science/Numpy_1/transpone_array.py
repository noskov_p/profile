import numpy as np

A = np.array([[2, 9, 8, 0],
             [8, 7, 5, 6],
             [3, 1, 0, 2],
             [6, 5, 6, 8],
             [1, 0, 0, 4]])

line = len(A[0]) # сколько строк будет в новом
column = len(A)    # сколько столбцов будет в новом
A_tr = np.zeros((line, column), int)

for i in range(line):
     A_tr[i] += A[:, i]
print(A_tr)
