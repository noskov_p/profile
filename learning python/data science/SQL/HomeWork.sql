-- Домашнее задание к уроку 15.3: 

-- 1. Написать запрос для выбора студентов в порядке их регистрации на сервисе.
USE skillbox;
SELECT * FROM `students` ORDER BY `registration_date`;
-- 2. Написать запрос для выбора 5 самых дорогих курсов, на которых более 4 студентов, и которые длятся более 10 часов.
SELECT * FROM `courses` WHERE `students_count` > 4 ORDER BY `price` DESC LIMIT 5;
-- 3. Написать один (!) запрос, который выведет одновременно список из имен трёх самых молодых студентов, имен трёх самых старых учителей и названий трёх самых продолжительных курсов.
(SELECT `name`, `age` FROM `students` ORDER BY `age` LIMIT 3)
UNION
(SELECT `name`, `age` FROM `teachers` ORDER BY `age` DESC LIMIT 3)
UNION
(SELECT `name`, `duration` FROM `courses` ORDER BY `duration` DESC LIMIT 3);

-- Домашнее задание к уроку 15.4:

-- 1. Написать запрос для выбора среднего возраста всех учителей с зарплатой более 10 000.
SELECT AVG(`age`) FROM `teachers` WHERE `salary` > 10000;
-- 2. Написать запрос для расчета суммы, сколько будет стоить купить все курсы по дизайну.
SELECT SUM(`price`) FROM `courses` WHERE `type` = 'DESIGN';
-- 3. Написать запрос для расчёта, сколько минут (!) длятся все курсы по программированию.
SELECT SUM(`duration`)*60 FROM `courses` WHERE `type` = 'PROGRAMMING';


-- Домашнее задание к уроку 15.6:

-- 1. Напишите запрос, который выводит сумму, сколько часов должен в итоге проучиться каждый студент (сумма длительности всех курсов на которые он подписан).
-- В результате запрос возвращает две колонки: Имя Студента — Кол-во часов.
SELECT `st`.`name`, SUM(`cu`.`duration`) AS `sum_duration`
FROM `students` AS `st`
INNER JOIN `subscriptions` AS `sc` ON `st`.`id` = `sc`.`student_id`
INNER JOIN `courses` AS `cu` ON `sc`.`course_id` = `cu`.`id`
GROUP BY `st`.`name`;

-- 2. Напишите запрос, который посчитает для каждого учителя средний возраст его учеников.
-- В результате запрос возвращает две колонки: Имя Учителя — Средний Возраст Учеников.
SELECT `tc`.`name`, AVG(`st`.`age`) AS `avg_age_st`
FROM `teachers` AS `tc`
INNER JOIN `courses` AS `cr` ON `tc`.`id` = `cr`.`teacher_id`
INNER JOIN `subscriptions` AS `sc` ON `cr`.`id` = `sc`.`course_id`
INNER JOIN `students` AS `st` ON `sc`.`student_id` = `st`.`id`
GROUP BY `tc`.`name`;

-- 3. Напишите запрос, который выводит среднюю зарплату учителей для каждого типа курса (Дизайн/Программирование/Маркетинг и т.д.).
-- В результате запрос возвращает две колонки: Тип Курса — Средняя зарплата.
SELECT DISTINCT `cr`.`type`, AVG(`tc`.`salary`) AS `avg_salary_teachers`
FROM `courses` AS `cr`
INNER JOIN `teachers` AS `tc` ON `tc`.`id` = `cr`.`teacher_id`
GROUP BY `cr`.`type`;

