import numpy as np
array = np.array([[1, 2, 3, 4],
                  [5, 6, 7, 8],
                  [9, 10, 11, 12]])

def my_reshape(array_in, line, column):
    array_in = array.ravel()
    array_out = np.zeros((line, column), int)
    j = 0
    try:
        for index_list in range(line):
            for index_elem in range(column):
                array_out[index_list][index_elem] = array_in[j]
                j += 1
    except  IndexError:
        return np.array([])
    return array_out

print(my_reshape(array, 6, 2))