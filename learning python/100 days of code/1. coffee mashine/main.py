from menu import Menu
from coffee_maker import CoffeeMaker
from money_machine import MoneyMachine

coffee_maker = CoffeeMaker()
menu = Menu()
money_machine = MoneyMachine()

works = True

while works:
    order = input("Что бы вы хотели? (espresso / latte / cappuccino)")
    if order in ("espresso", "latte", "cappuccino"):
        coffee_order = menu.find_drink(order)
        if coffee_maker.is_resource_sufficient(coffee_order):
            if money_machine.make_payment(coffee_order.cost):
                coffee_maker.make_coffee(coffee_order)
    elif order == "выкл":
        works = False
    elif order == "отчет":
        coffee_maker.report()
    else:
        print("Некорректный ввод. Попробуйте еще раз!")
