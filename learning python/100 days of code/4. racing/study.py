from turtle import Turtle, Screen

tim = Turtle()
screen = Screen()

def w_forwards():
    tim.forward(20)

def s_backwards():
    tim.back(20)

def a_left():
    tim.left(5)

def d_right():
    tim.right(5)

def c_reset():
    tim.reset()

screen.listen()

screen.onkey(fun=w_forwards, key="w")
screen.onkey(fun=s_backwards, key="s")
screen.onkey(fun=a_left, key="a")
screen.onkey(fun=d_right, key="d")
screen.onkey(fun=c_reset, key="c")



screen.exitonclick()