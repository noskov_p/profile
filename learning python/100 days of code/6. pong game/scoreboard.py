import turtle
ALIAGMENT = "center"
FONT = ("Arial", 24, "normal")


class Scoreboard(turtle.Turtle):

    def __init__(self):
        super().__init__()
        self.l_score = 0
        self.r_score = 0
        self.color("white")
        self.up()
        self.goto(0, 260)
        self.hideturtle()
        self.write_score()

    def write_score(self):
        self.clear()
        self.write(f"{self.l_score} : {self.r_score}", move=False, align=ALIAGMENT, font=FONT)

    def game_over(self):
        self.goto(0, 0)
        self.write(f"Game Over!", move=False, align=ALIAGMENT, font=FONT)

    def l_point(self):
        self.l_score += 1
        self.write_score()

    def r_point(self):
        self.r_score += 1
        self.write_score()
