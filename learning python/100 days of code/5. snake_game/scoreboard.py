import turtle
ALIAGMENT = "center"
FONT = ("Arial", 24, "normal")


class Scoreboard(turtle.Turtle):

    def __init__(self):
        super().__init__()
        self.score = 0
        self.high_score = self.read_high_score()
        self.color("white")
        self.up()
        self.goto(0, 260)
        self.hideturtle()
        self.write_score()

    def read_high_score(self):
        with open("../../../../../Desktop/data.txt") as file:
        #with open("/Users/pnoskov/Desktop/data.txt") as file:
            high_score = int(file.read())
        return high_score

    def write_high_score(self):
        with open("/Users/pnoskov/Desktop/data.txt", mode="w") as file:
            file.write(f"{self.high_score}")


    def write_score(self):
        self.clear()
        self.write(f"Score: {self.score} High Score {self.high_score}", move=False, align=ALIAGMENT, font=FONT)

    def reset(self):
        if self.score > self.high_score:
            self.high_score = self.score
            self.write_high_score()
        self.score = 0
        self.write_score()

    def increment(self):
        self.score += 1
        self.write_score()

