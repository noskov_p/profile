# numbers = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
#
# squared_numbers = [ n**2 for n in numbers]
#
# print(squared_numbers)

# numbers = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
#
# result = [n for n in numbers if n % 2 == 0]
#
# print(result)

# with open("file1.txt") as file_1:
#     list_1 = [line.rstrip() for line in file_1.readlines()]
# with open("file2.txt") as file_2:
#     list_2 = [line.rstrip() for line in file_2.readlines()]#
# result = [int(number) for number in list_1 if number in list_2]#
# print(result)

# import random
# names = ["Alex", "Beth", "Caroline", "Dave", "Eleanor", "Freddie"]
# students_scores = {name:random.randint(1, 100) for name in names}
# print(students_scores)
# passed_students = { name:score for (name, score) in students_scores.items() if score > 60}
# print(passed_students)

# sentence = "What is the Airspeed Velocity of an Unladen Swallow?"
# result = {value : len(value) for value in sentence[:-1].split()}
# print(result)

# weather_c = {
#     "Monday": 12,
#     "Tuesday": 14,
#     "Wednesday": 15,
#     "Thursday": 14,
#     "Friday": 21,
#     "Saturday": 22,
#     "Sunday": 24,
# }
# weather_f = {key : value * 9 / 5 + 32 for (key, value) in weather_c.items()}
# print(weather_f)

student_dict = {
    "student" : ["Angela", "James", "Lily"],
    "score" : [56, 76, 98]
}

# for (key, value) in student_dict.items():
#     print(key, value)

import pandas

student_data_frame = pandas.DataFrame(student_dict)
# print(student_data_frame)
# for (key, value) in student_data_frame.items():
#     print(key)
#     print(value)
for (index, row) in student_data_frame.iterrows():
    print(index)
    print(row.student)

