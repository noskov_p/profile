#TODO: Create a letter using starting_letter.docx 
#for each name in invited_names.txt
#Replace the [name] placeholder with the actual name.
#Save the letters in the folder "ReadyToSend".

# Имена в массив
with open("Input/Names/invited_names.txt") as file_names:
    names = file_names.readlines()
    names = [name.rstrip() for name in names]
    # ['Aang', 'Zuko', 'Appa', 'Katara', 'Sokka', 'Momo', 'Uncle Iroh', 'Toph']

# Строчки письма в массив
with open("Input/Letters/starting_letter.txt") as file_letter:
    letter = file_letter.readlines()
    # Пишем письма для каждого имени
    for name in names:
        with open(f"Output/ReadyToSend/letter_for_{name}.txt", mode="w") as completed_letter:
            for string in letter:
                completed_letter.write(string.replace("[name]", name))
