from tkinter import *

window = Tk()
window.title("My first GUI Program")
window.minsize(width=500, height=300)

# Label
my_label = Label(text="I am a Label", font=("Arial", 24, "bold")) # "italic", отсутствие - обычный
my_label.pack() # side="left" - расположить слева, expand=True - все доступное пространство
my_label["text"] = "New text"
my_label.config(text="New text")

# button
# count = 0
def button_clicked():
    # global count
    # my_label.config(text= f"Меня кликнули {count} раз!")
    # count += 1
    my_label.config(text=input.get())

button = Button(text="Click me", command=button_clicked)
button.pack()

# Entry
input = Entry(width=10)
input.pack()


window.mainloop()