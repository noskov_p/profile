def add(*args):
    sum = 0
    for number in args:
        sum += number
    return sum

print(add(1,2,3,4,5,6,7))


def calculate(n, **kwargs):
    # Так будет ошибка при не передаче аргумента
    # n += kwargs["add"]
    # n *= kwargs["multiply"]
    # Так будет None при не передаче аргумента
    n += kwargs.get("add")
    n *= kwargs.get("multiply")
    print(n)

calculate(2, add=3, multiply=5)