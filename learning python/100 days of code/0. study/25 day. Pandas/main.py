# with open("weather-data.csv") as data_file:
#     data = data_file.readlines()
#     print(data)

# import csv
# with open("weather-data.csv") as data_file:
#     data = csv.reader(data_file)
#     temperatures = []
#     for row in data:
#         if row[1] != "temp":
#             temperatures.append(int(row[1]))
#
#     print(temperatures)

import pandas

# data = pandas.read_csv("weather-data.csv")

# print(data["condition"])
# print(data.condition)
# print(data[data.temp == data.temp.max()])
# monday = data[data.day == "Monday"]
# monday_temp_F = monday.temp * 9 / 5 +32
# print(monday_temp_F)

# data_dict = {
#     "students" : ["Any", "James", "Angela"],
#     "scores" : [76, 56, 65]
# }
# dataFrame = pandas.DataFrame(data_dict)
# print(dataFrame)
# dataFrame.to_csv("dataFrame.csv")

data = pandas.read_csv("squirrel_data.csv")

# print(data["Primary Fur Color"].value_counts())
#
# data["Primary Fur Color"].value_counts().to_csv("squirrel_count.csv")

grey_squirrels_count = len(data[data["Primary Fur Color"] == "Gray"])
red_squirrels_count = len(data[data["Primary Fur Color"] == "Cinnamon"])
black_squirrels_count = len(data[data["Primary Fur Color"] == "Black"])

# print(grey_squirrels_count, red_squirrels_count, black_squirrels_count)

data_dict = {
    "Fur Color" : ["Gray", "Cinnamon", "Black"],
    "Count" : [grey_squirrels_count, red_squirrels_count, black_squirrels_count]
}

dataFrame = pandas.DataFrame(data_dict)
dataFrame.to_csv("squirrel_count.csv")