import random
import turtle
turtle.colormode(255)

color_list = [(132, 166, 205), (221, 148, 107), (31, 42, 62), (198, 135, 148), (165, 58, 48), (141, 184, 162), (39, 105, 156), (237, 212, 91), (149, 60, 67), (215, 83, 72), (235, 166, 157), (51, 111, 90), (167, 30, 33), (35, 61, 55), (156, 33, 31), (17, 97, 71), (52, 44, 49), (229, 162, 166), (170, 188, 221), (57, 51, 48), (31, 60, 109), (180, 105, 115), (106, 126, 159), (175, 200, 188), (35, 150, 209), (65, 66, 56)]
tim = turtle.Turtle()

tim.up()
tim.goto(-200, -200)
for _ in range(10):
    for _ in range(10):
        tim.dot(20, random.choice(color_list))
        tim.forward(50)
        print(tim.xcor())
    tim.goto(tim.xcor() - 500, tim.ycor() + 50)

screen = turtle.Screen()
screen.exitonclick()
