import turtle
from turtle import Turtle, Screen
import random

turtle.colormode(255)

timmy = Turtle()
#timmy.shape("turtle")


# count_corner = 3
# for _ in range(6):
#     for _ in range(count_corner):
#         timmy.forward(100)
#         timmy.right(360/count_corner)
#     count_corner += 1
def random_color():
    r = random.randint(0,255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    return (r, g, b)

agle = 0
timmy.speed(20)


#timmy.pensize(15)
def draw_spirograph(size_of_gap):
    for _ in range(360 //  size_of_gap):
        timmy.color(random_color())
        timmy.circle(100)
        timmy.setheading(timmy.heading() + size_of_gap)

draw_spirograph(5)

screen = Screen()
screen.exitonclick()


