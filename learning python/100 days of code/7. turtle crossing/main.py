import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

# Чем больше, тем реже создаются машины
FREQUENCY_OF_CREATION = 5

screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)

player = Player()
car_manager = CarManager()
scoreboard = Scoreboard()

screen.listen()
screen.onkey(player.go_up, "Up")

i = 1
game_is_on = True
while game_is_on:
    time.sleep(0.2)
    screen.update()

    # Чтобы машины создавались реже
    if i % FREQUENCY_OF_CREATION == 0:
        car_manager.create_cars()
    car_manager.drive()
    i += 1

    # Распознование столкновение с машиной
    for car in car_manager.all_cars:
        if car.distance(player) < 20:
            game_is_on = False
            scoreboard.game_over()

    # Достигли финиша
    if player.is_at_finish():
        player.go_to_start()
        car_manager.level_up()
        scoreboard.level_up()

screen.exitonclick()
