import turtle
ALIAGMENT = "center"
FONT = ("Arial", 24, "normal")


class Scoreboard(turtle.Turtle):

    def __init__(self):
        super().__init__()
        self.level = 0
        self.color("black")
        self.up()
        self.goto(-200, 260)
        self.hideturtle()
        self.write_score()

    def write_score(self):
        self.clear()
        self.write(f"LEVEL: {self.level} ", move=False, align=ALIAGMENT, font=FONT)

    def game_over(self):
        self.goto(0, 0)
        self.write(f"Game Over!", move=False, align=ALIAGMENT, font=FONT)

    def level_up(self):
        self.level += 1
        self.write_score()


