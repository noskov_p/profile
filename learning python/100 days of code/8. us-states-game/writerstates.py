import turtle
ALIAGMENT = "center"
FONT = ("Arial", 12, "normal")


class WriterStates(turtle.Turtle):
    """
        Пишет штаты на карте
    """
    def __init__(self):
        super().__init__()
        self.state_count = 50
        self.color("black")
        self.up()
        self.hideturtle()

    def write_state(self, str, x, y):
        self.goto(x, y)
        self.write(f"{str}", move=False, align=ALIAGMENT, font=FONT)



