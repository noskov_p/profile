import turtle
ALIAGMENT = "center"
FONT = ("Arial", 16, "normal")


class Scoreboard(turtle.Turtle):
    """
        Счетчик угаданных штатов
    """

    def __init__(self):
        super().__init__()
        self.state_count = 50
        self.color("black")
        self.up()
        self.goto(0, 245)
        self.hideturtle()
        self.write_state_count()

    def write_state_count(self):
        self.clear()
        self.write(f"{self.state_count} : 50 ", move=False, align=ALIAGMENT, font=FONT)

    def state_count_down(self):
        self.state_count -= 1
        self.write_state_count()


