import turtle
import pandas
from scoreboard import Scoreboard
from writerstates import WriterStates
import numpy as np

screen = turtle.Screen()
screen.title("U.S. States Game")
image = "blank_states_img.gif"
screen.addshape(image)
turtle.shape(image)
scoreboard = Scoreboard()
writerstates = WriterStates()

 # def get_mouse_click_coor(x, y):
#     print(x, y)
# turtle.onscreenclick(get_mouse_click_coor)

data = pandas.read_csv("50_states.csv")
# data['state'] = data['state'].str.lower()
# all_states = data.state.to_list()
# print(data[data.state == "Ohio"].x.values[0]) - 176
#print(data[data.state == "Ohio"])
#    state    x   y
# 34  Ohio  176  52
# turtle.mainloop()
scoreboard.write_state_count()

is_game = True
while is_game:
    # Ввод пользователя
    answer_state = screen.textinput(title="Guess the State", prompt="What's another state's name?").title()

    if answer_state == "Exit":
        data.state.to_csv("wrong_states.csv")
        break
    # Получаем такой штат из фрейма
    answer_data = data[data.state == answer_state]
    # # Если есть такой штат
    if not answer_data.empty:
        # Координаты штата для надписи
        # Получим первый элемент серии координаты
        # x = answer_data.x.values[0]
        # x = int(answer_data.x)
        # Удаляем из фрейма угаданный штат
        data.drop(data[data.state == answer_state].index, inplace=True)
        # Переустановим индекс
        # data.reset_index(drop=True, inplace=True)
        # Переустановим индекс и сделаем его с единицы
        data = data.set_index(np.arange(1, len(data)+1))
        x = answer_data.x.item()
        y = answer_data.y.item()
        scoreboard.state_count_down()
        writerstates.write_state(answer_state, x, y)




